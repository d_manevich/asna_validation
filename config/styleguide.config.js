const path = require('path')

module.exports = {
  webpackConfig: require('./webpack.config.dev'),
  components: '../src/components/**/*.{js,jsx,ts,tsx}',
  styleguideComponents: {
    Wrapper: 'react-router-dom/BrowserRouter'
  },
  require: [
    path.join(__dirname, '../src/index.css'),
    path.join(__dirname, '../src/assets/fonts/fonts.css')
  ]
}