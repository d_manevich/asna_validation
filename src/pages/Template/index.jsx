import { connect } from 'react-redux'
import { getTemplate } from '@/store/templates/templatesSelectors'
import { loadTemplate, createTemplate, updateTemplate } from '@/store/templates/templatesActions'
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import Template from './Template'

const mapStateToProps = (state, { match }) => ({
  template: getTemplate(state, parseInt(match.params.id))
})

const mapDispatchToProps = dispatch => ({
  onLoad: template => dispatch(loadTemplate(template)),
  onCreate: template => dispatch(createTemplate(template)),
  onUpdate: template => dispatch(updateTemplate(template))
})

class TemplateContainer extends PureComponent {
  static propTypes = {
    onLoad: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string
      }).isRequired
    }).isRequired
  }

  get id () {
    return this.props.match.params.id
  }

  state = {
    isLoading: !!this.id,
    notFound: false
  }

  componentDidMount () {
    if (this.id) {
      axios.get(`/api/templates/${this.id}`)
        .then(response => response.data.data)
        .then(template => this.props.onLoad(template))
        .catch(() => this.setState({ notFound: true }))
        .then(() => this.setState({ isLoading: false }))
    }
  }

  handleCreateTemplate = template => {
    axios.post('/api/templates', {template})
      .then(response => response.data.data)
      .then(template => this.props.onCreate(template))
      .then(() => this.props.history.push('/templates'))
  }

  handleUpdateTemplate = template => {
    axios.put(`/api/templates/${this.id}`, { template })
      .then(response => response.data.data)
      .then(template => this.props.onUpdate(template))
      .then(() => this.props.history.push('/templates'))
  }

  render () {
    return <Template
      {...this.props}
      {...this.state}
      onCreate={this.handleCreateTemplate}
      onUpdate={this.handleUpdateTemplate}
      isLoading={this.state.isLoading}
      notFound={this.state.notFound}
    />
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplateContainer)
