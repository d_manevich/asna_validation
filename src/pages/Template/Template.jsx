import React, { PureComponent, Fragment } from 'react'
import { remove, insert, update, omit } from 'ramda'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Link from '@/components/Link'
import Input from '@/components/Input'
import Button from '@/components/Button'
import Select from '@/components/Select'
import ColumnList from '@/containers/ColumnList'
import ColumnSettings from '@/components/ColumnSettings'
import NewColumnModal from '@/components/NewColumnModal'
import { initialColumnState } from '@/store/templates/templatesReducer'

const OPTIONS = [
  { label: 'Товародвижение', value: 'supply_chain' },
  { label: 'Остатки', value: 'balances' }
]

const swapUp = (from, to, list) => insert(to, list[from], remove(from, 1, list))
const swapDown = (from, to, list) => remove(from, 1, insert(to + 1, list[from], list))

export default class Template extends PureComponent {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string
      }).isRequired
    }).isRequired,
    template: PropTypes.object.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    notFound: PropTypes.bool.isRequired
  }

  state = {
    ...this.props.template,
    columnModalOpen: false
  }

  handleNameChange = event => {
    this.setState({ name: event.target.value })
  }

  handleTypeChange = type => {
    this.setState({ type })
  }

  handleOpenModal = () => {
    this.setState({ columnModalOpen: true })
  }

  handleCloseModal = () => {
    this.setState({ columnModalOpen: false })
  }

  handleToggle = index => {
    this.setState(prevState => {
      const column = prevState.columns[index]

      return {
        columns: update(index, { ...column, active: !column.active }, prevState.columns)
      }
    })
  }

  handleCreateColumn = name => {
    this.setState(prevState => ({
      columns: [ ...prevState.columns, { ...initialColumnState, name } ]
    }))
  }

  handleDelete = delIndex => {
    this.setState(prevState => ({
      columns: remove(delIndex, 1, prevState.columns)
    }))
  }

  handleSort = (from, to) => {
    if (from < to) {
      this.setState({ columns: swapDown(from, to, this.state.columns) })
    }

    if (from > to) {
      this.setState({ columns: swapUp(from, to, this.state.columns) })
    }
  }

  handleRequirementsChange = (index, requirements) => {
    this.setState(prevState => ({
      columns: update(index, { ...prevState.columns[index], requirements }, prevState.columns)
    }))
  }

  handleToggleOptional = (index, optional) => {
    this.setState(prevState => ({
      columns: update(index, { ...prevState.columns[index], optional }, prevState.columns)
    }))
  }

  handleSave = (event) => {
    event.preventDefault()
    const { match, onCreate, onUpdate } = this.props
    const template = omit(['columnModalOpen', 'id', 'columns'], this.state)
    const columns = this.state.columns.map((column, index) => ({ ...column, order: index }))

    if (match.params.id) {
      onUpdate({ ...template, columns })
    } else {
      onCreate({ ...template, columns })
    }
  }

  render () {
    const { isLoading, notFound } = this.props
    const { name, type, columns } = this.state
    const columnsNames = columns.map(column => column.name)

    return (
      <Wrapper>
        <Header>
          <Anchor to='#name'>Название шаблона</Anchor>
          <Anchor to='#criteria'>Критерии чистоты</Anchor>
          <Anchor to='#settings'>Настройка параметров</Anchor>
        </Header>

        { notFound && <NotFound>Шаблон не найден</NotFound> }

        { isLoading && <Loading>Загрузка...</Loading> }

        { !isLoading && !notFound &&
          <Fragment>
            <Container>
              <NameForm id='name' onSubmit={this.handleSave}>
                <Label>
                  Название шаблона
                  <NameInput placeholder='Шаблон 1' value={name} onChange={this.handleNameChange} />
                </Label>
                <Button color='primary'>Сохранить</Button>
              </NameForm>
              <Label>
                Выберите тип:
                <TypeSelect value={type} options={OPTIONS} onChange={this.handleTypeChange} />
              </Label>
              <ColumsHeader id='name'>
                <div>Выберите столбцы, применимые к проверке, а затем настройте элементы проверки.</div>
                <ColumsHint>Перетащите столбцы, чтобы создать нужный порядок</ColumsHint>
              </ColumsHeader>
              <ColumnList
                columns={columns}
                onToggle={this.handleToggle}
                onCreate={this.handleOpenModal}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
            </Container>
            <NewColumnModal
              isOpen={this.state.columnModalOpen}
              onClose={this.handleCloseModal}
              onSave={this.handleCreateColumn}
            />
            {columns.map((column, index) => (
              <ColumnSettings
                number={index + 1}
                name={column.name}
                requirements={column.requirements}
                columns={columnsNames}
                optional={column.optional}
                key={index}
                onChange={requirements => this.handleRequirementsChange(index, requirements)}
                onToggleOptional={optional => this.handleToggleOptional(index, optional)}
              />
            ))}
            <SaveBtn
              onClick={this.handleSave}
              color='primary'
              size='xlg'
            >
              Сохранить
            </SaveBtn>
          </Fragment>
        }
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  padding: 45px 0 350px;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Header = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 45px;
`

const Anchor = styled(Link)`
  margin-left: 50px;
`

const Loading = styled.div`
  font-size: 24px;
  text-align: center;
`

const NotFound = styled.div`
  font-size: 24px;
  text-align: center;
`

const Container = styled.div`
  width: 100%;
  background-color: white;
  border-radius: 3px;
  padding: 90px 115px 95px 95px;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
  margin-bottom: 55px;
`

const NameForm = styled.form`
  display: flex;
  align-items: flex-end;
  margin-bottom: 40px;
`

const Label = styled.label`
  display: flex;
  flex-direction: column;
  width: 310px;
  margin-right: 10px;
`

const NameInput = styled(Input)`
  margin-top: 10px;
`

const TypeSelect = styled(Select)`
  margin-top: 10px;
`

const ColumsHeader = styled.div`
  margin-top: 25px;
  margin-bottom: 20px;
`

const ColumsHint = styled.div`
  color: #BFBFBF;
`

const SaveBtn = styled(Button)`
  margin-top: 30px;
  margin-left: auto;
  width: 350px;
`
