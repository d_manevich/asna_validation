import React, { PureComponent } from 'react'
import { find } from 'ramda'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Tabs from '@/components/Tabs'
import Link from '@/components/Link'
import TemplateRow from '@/components/TemplateRow'

const OPTIONS = [
  { label: 'Товародвижение', value: 'supply_chain' },
  { label: 'Остатки', value: 'balances' }
]

export default class Templates extends PureComponent {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    templates: PropTypes.array.isRequired,
    onToggle: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
  }

  state = {
    templateType: 'supply_chain'
  }

  get selectedType () {
    const option = find(option => option.value === this.state.templateType, OPTIONS)

    return option ? option.label : ''
  }

  getTemplatesByType = type => this.props.templates.filter(template => template.type === type)

  handleTabChange = templateType => {
    this.setState({ templateType })
  }

  render () {
    const { isLoading, onToggle, onDelete } = this.props
    const { templateType } = this.state

    return (
      <Wrapper>
        <Header>
          <Label>Выберите тип шаблонов</Label>
          <Tabs
            value={this.state.templateType}
            tabs={OPTIONS}
            onChange={this.handleTabChange}
          />
        </Header>
        <Container>
          <ListHeader>
            Шаблоны для файлов типа «{this.selectedType}»
            <Link to='/templates/new' type='button'>Создать новый шаблон</Link>
          </ListHeader>

          {isLoading && <Loading>Загрузка...</Loading>}

          {!isLoading && this.getTemplatesByType(templateType).map(template => (
            <TemplateRow
              template={template}
              onDelete={() => onDelete(template.id)}
              onEnable={() => onToggle(template.id, true)}
              onDisable={() => onToggle(template.id, false)}
              key={template.id}
            />
          ))}
        </Container>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  padding-top: 35px;
  padding-bottom: 80px;
`

const Header = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 35px;
`

const Label = styled.div`
  margin-right: 35px;
`

const Container = styled.div`
  width: 100%;
  background-color: white;
  border-radius: 3px;
  padding: 35px 30px 70px;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
`

const ListHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 35px;
`

const Loading = styled.div`
  font-size: 24px;
  text-align: center;
`
