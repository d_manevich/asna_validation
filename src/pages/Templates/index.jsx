import { connect } from 'react-redux'
import { getTemplates } from '@/store/templates/templatesSelectors'
import {
  loadTemplates,
  updateTemplate,
  deleteTemplate
} from '@/store/templates/templatesActions'
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import Templates from './Templates'

const mapStateToProps = state => ({
  templates: getTemplates(state)
})

const mapDispatchToProps = dispatch => ({
  onLoad: templates => dispatch(loadTemplates(templates)),
  onUpdate: template => dispatch(updateTemplate(template)),
  onDelete: id => dispatch(deleteTemplate(id))
})

class TemplatesContainer extends PureComponent {
  static propTypes = {
    onLoad: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
  }

  state = {
    isLoading: true
  }

  componentDidMount () {
    this.loadTemplates()
  }

  loadTemplates = () => {
    axios.get('/api/templates')
      .then(response => response.data.data)
      .then(templates => this.props.onLoad(templates))
      .then(() => this.setState({ isLoading: false }))
  }

  handleDelete = id => {
    axios.delete(`/api/templates/${id}`)
      .then(() => this.props.onDelete(id))
  }

  handleToggle = (id, active) => {
    axios.patch(`/api/templates/${id}`, { template: { active } })
      .then(() => this.loadTemplates())
  }

  render () {
    return (
      <Templates
        {...this.props}
        {...this.state}
        onDelete={this.handleDelete}
        onToggle={this.handleToggle}
      />
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplatesContainer)
