import { connect } from 'react-redux'
import {
  hasActiveBalancesTemplate,
  hasActiveSupplyChainTemplate
} from '@/store/templates/templatesSelectors'
import {
  startUploadFiles,
  changeUploadProgress, // eslint-disable-line
  finishUploadFiles
} from '@/store/loader/loaderActions'
import axios from 'axios'
import Upload from './Upload'

const mapStateToProps = state => ({
  accept: 'text/plain, text/csv',
  canUpload: hasActiveBalancesTemplate(state) && hasActiveSupplyChainTemplate(state)
})

const mapDispatchToProps = (dispatch, { history }) => ({
  onUpload: acceptedFiles => {
    history.push('/loader')

    const filenames = acceptedFiles.map(file => file.name)
    dispatch(startUploadFiles(filenames))

    const formData = new FormData()
    acceptedFiles.forEach(file => formData.append('files[]', file, file.name))
    axios.post('/api/upload', formData, {
      onUploadProgress: ({ loaded, total }) => {
        dispatch(changeUploadProgress(filenames, Math.round(loaded * 100 / total)))
      }
    })
      .catch(() => {})
      .then(() => dispatch(finishUploadFiles(filenames)))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Upload)
