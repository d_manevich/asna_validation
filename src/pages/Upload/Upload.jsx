import React from 'react'
import PropTypes from 'prop-types'
import Dropzone from 'react-dropzone'
import Button from '@/components/Button'
import styled from 'styled-components'
import '@/pages/Upload/dropzone.module.css'

const Upload = ({ onUpload, accept, canUpload }) => (
  <Wrapper>
    { canUpload &&
      <Container>
        <Title>Добавить файлы на проверку</Title>
        <Dropzone
          className='dropzone'
          activeClassName='dropzone--active'
          rejectClassName='dropzone--reject'
          accept={accept}
          onDropAccepted={onUpload}
        >
          Перетащите сюда файлы или
          <StyledButton color='secondary'>Загрузите с компьютера</StyledButton>
        </Dropzone>
      </Container>
    }
    { !canUpload &&
      <Container>
        <Title>Нет активных шаблонов</Title>
      </Container>
    }
  </Wrapper>
)

Upload.propTypes = {
  onUpload: PropTypes.func.isRequired,
  canUpload: PropTypes.bool.isRequired,
  accept: PropTypes.string
}

export default Upload

const Wrapper = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const Container = styled.div`
  width: 670px;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
  border-radius: 3px;
  background-color: white;
  padding: 50px 60px 70px 65px;
`

const Title = styled.div`
  font-size: 32px;
  margin-bottom: 35px;
`

const StyledButton = styled(Button)`
  margin-left: 20px;
  padding: 0 12px;
`
