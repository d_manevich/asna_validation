import { connect } from 'react-redux'
import { getLogs } from '@/store/logs/logsSelectors'
import Logs from './Logs'

const mapStateToProps = state => ({
  logs: getLogs(state)
})

export default connect(mapStateToProps)(Logs)
