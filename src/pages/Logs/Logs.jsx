import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Logs = ({ logs }) => (
  <Wrapper>
    <Container>
      <Title>Загруженные файлы</Title>
      <Table>
        <thead>
          <tr>
            <Th>Пользователь</Th>
            <Th>Файл</Th>
            <Th>Шаблон</Th>
            <Th>Дата</Th>
          </tr>
        </thead>
        <tbody>
          {logs.map((log, index) => (
            <tr key={index}>
              <Td>{log.username}</Td>
              <Td>{log.filename}</Td>
              <Template valid={log.valid}>{log.template}</Template>
              <Td>{log.date}</Td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  </Wrapper>
)

Logs.propTypes = {
  logs: PropTypes.array.isRequired
}

export default Logs

const Wrapper = styled.div`
  padding: 100px 0;
`

const Container = styled.div`
  background-color: white;
  border-radius: 3px;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
  padding: 35px 35px 35px 25px;
`

const Title = styled.div`
  font-size: 30px;
  margin-bottom: 25px;
`

const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
`

const Th = styled.th`
  font-size: 16px;
  line-height: 30px;
  color: black;
  font-weight: 400;
  opacity: 0.4;
  text-align: left;
  border-bottom: 2px solid #979797;
`

const Td = styled.td`
  font-size: 16px;
  line-height: 44px;
  font-size: 16px;
  font-weight: 400;
  color: black;
  border-bottom: 1px solid #979797;
`

const Template = Td.extend`
  color: ${props => props.valid ? '#38EA8A' : '#F02020'};
`
