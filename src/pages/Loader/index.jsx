import { connect } from 'react-redux'
import { getUploadedFiles } from '@/store/loader/loaderSelectors'
import Loader from './Loader'

const FILES = [
  { name: 'Org_JELE_JELE8_JELE_1_20171213dsad', percent: 60 },
  { name: 'Org_JELE_JELE8_JELE_1_2123dd231dd1', percent: 40 },
  { name: 'Org_JELE_JELE8_JELE_1_12d4d2123311', percent: 10 },
  { name: 'Org_JELE_JELE8_JELE_1_13d12332133d', percent: 98 },
  { name: 'Org_JELE_JELE8_JELE_1_2sasd13dasd2', percent: 45 },
  { name: 'Org_JELE_JELE8_JELE_1_2017asdsa213', percent: 1 },
  { name: 'Org_JELE_JELE8_JELE_1_20adasd133sd', percent: 0 }
]

const mapStateToProps = state => ({
  uploadedFiles: getUploadedFiles(state),
  processedFiles: FILES
})

export default connect(mapStateToProps)(Loader)
