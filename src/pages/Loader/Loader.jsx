import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Tabs from '@/components/Tabs'
import LoaderRow from '@/components/LoaderRow'
import VerifiedRow from '@/components/VerifiedRow'
import Button from '@/components/Button'
import Link from '@/components/Link'

export default class Loader extends PureComponent {
  static propTypes = {
    uploadedFiles: PropTypes.array.isRequired,
    processedFiles: PropTypes.array.isRequired
  }

  state = {
    tab: 'upload',
    checkedFiles: [
      { id: '23213abf123', name: 'Org_JELE_JELE8_JELE_1_20171102T2231', checked: true, valid: true },
      { id: '13213abf123', name: 'Org_JELE_JELE8_JELE_1_20171102T2231', checked: false, valid: true },
      { id: '1321fabf123', name: 'Org_JELE_JELE8_JELE_1_20171102T2231', checked: false, valid: false }
    ]
  }

  get tabs () {
    const { uploadedFiles, processedFiles } = this.props
    const { checkedFiles } = this.state
    return [
      { label: `Загрузка: ${uploadedFiles.length}`, value: 'upload' },
      { label: `В обработке: ${processedFiles.length}`, value: 'processing' },
      { label: `Проверено: ${checkedFiles.length}`, value: 'verified' }
    ]
  }

  get files () {
    switch (this.state.tab) {
      case 'upload':
        return this.props.uploadedFiles

      case 'processing':
        return this.props.processedFiles

      case 'verified':
        return this.state.checkedFiles

      default:
        return []
    }
  }

  handleChangeTab = tab => {
    this.setState({ tab })
  }

  handleVerifiedChange = (id, value) => {
    this.setState(prevState => ({
      checkedFiles: prevState.checkedFiles.map(file => (
        file.id === id ? { ...file, checked: value } : file
      ))
    }))
  }

  render () {
    const { checkedFiles, tab } = this.state
    const isVerified = tab === 'verified'

    return (
      <Wrapper>
        <Header>
          <Tabs tabs={this.tabs} value={tab} onChange={this.handleChangeTab} />
          {isVerified &&
            <Legend>
              <LegendItem valid>Без ошибок</LegendItem>
              <LegendItem>Ошибка</LegendItem>
            </Legend>
          }
          {!isVerified &&
            <AddLink to='/loader/upload' type='button'>Добавить</AddLink>
          }
        </Header>
        {!isVerified && this.files.map(file => (
          <LoaderRow
            name={file.name}
            progress={file.progress}
            type={tab}
            key={file.name}
          />
        ))}
        {isVerified && checkedFiles.map(file => (
          <VerifiedRow
            name={file.name}
            checked={file.checked}
            valid={file.valid}
            onChange={value => this.handleVerifiedChange(file.id, value)}
            key={file.id}
          />
        ))
        }
        {isVerified &&
          <VerifiedFooter>
            <UploadAll color='primary' size='lg'>
              Загрузить в экосистему все файлы, прошедшие проверку
            </UploadAll>
            <UploadSelected size='lg'>Загрузить выбранные</UploadSelected>
          </VerifiedFooter>
        }
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  padding: 60px 0 65px;
`

const Header = styled.div`
  position: relative;
  margin-bottom: 45px;
  display: flex;
  justify-content: space-between;
`

const AddLink = styled(Link)`
  line-height: 32px;
  color: #4ED17F;
`

const Legend = styled.div`
  position: absolute;
  right: -50px;
`

const LegendItem = styled.div`
  display: flex;
  align-items: center;
  font-size: 12px;
  color: black;

  &:before {
    content: '';
    width: 10px;
    height: 10px;
    margin-right: 10px;
    border-radius: 50%;
    background-color: ${props => props.valid ? '#43C09F' : '#ED4D4D'};
  }
`

const VerifiedFooter = styled.div`
  position: relative;
  margin-top: 40px;
  padding-top: 40px;
  display: flex;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width 140px;
    border-top: 1px solid black;
  }
`

const UploadAll = styled(Button)`
  flex-grow: 2;
  padding: 0 20px;
`

const UploadSelected = styled(Button)`
  flex-grow: 1;
  padding: 0 20px;
  margin-left: 5px;
`
