import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import DragTypes from '@/constants/DragTypes'
import { DropTarget } from 'react-dnd'

const columnTarget = {
  hover: (props, monitor) => {
    const dragItem = monitor.getItem()
    if (!dragItem) return

    const dragIndex = dragItem.index
    const hoverIndex = props.index
    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return
    }

    props.onSort(dragIndex, hoverIndex)
    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex
  }
}

function collect (connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  }
}

class DropColumn extends PureComponent {
  static propTypes = {
    index: PropTypes.number.isRequired, // eslint-disable-line
    onSort: PropTypes.func.isRequired, // eslint-disable-line
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired
  }

  render () {
    const { connectDropTarget, isOver, children } = this.props

    return connectDropTarget(
      <div style={{ position: 'relative' }}>
        {children}
        {isOver &&
          <Overlay />
        }
      </div>
    )
  }
}

export default DropTarget(DragTypes.COLUMN, columnTarget, collect)(DropColumn)

const Overlay = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: white;
  border-radius: 2px;
  border: 1px dashed #4ED17F;
  padding: 55px 20px;
  display: flex;
  flex-direction: column;
`
