import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import DragColumnLayer from '@/containers/DragColumnLayer'
import DropColumn from '@/containers/DropColumn'
import DragColumn from '@/containers/DragColumn'
import Column from '@/components/Column'

export default class ColumnList extends PureComponent {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      active: PropTypes.bool.isRequired
    })).isRequired,
    onToggle: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onSort: PropTypes.func.isRequired
  }

  render () {
    const { columns, onToggle, onDelete, onCreate, onSort } = this.props
    return (
      <Wrapper>
        {columns.map((column, index) => (
          <ColumnWrapper key={index}>
            <DropColumn index={index} onSort={onSort}>
              <DragColumn index={index} column={column}>
                <Column
                  number={index + 1}
                  {...column}
                  onChange={() => onToggle(index)}
                  onDelete={() => onDelete(index)}
                />
              </DragColumn>
            </DropColumn>
          </ColumnWrapper>
        ))}
        <CreateNew onClick={onCreate}>
          Создать новый столбец
        </CreateNew>
        <DragColumnLayer />
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
  margin: -5px;
  display: flex;
  flex-wrap: wrap;
`

const ColumnWrapper = styled.div`
  margin: 5px;
`

const CreateNew = styled.button`
  cursor: pointer;
  outline: none;
  height: 200px;
  width: 133px;
  background-color: transparent;
  border-radius: 2px;
  border: 3px solid #6CDA95;
  padding: 55px 20px;
  line-height: 20px;
  margin: 5px;
  font-size: 16px;
  font-weight: 400;
  display: flex;
  flex-direction: column;
`
