import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import DragTypes from '@/constants/DragTypes'
import { DragSource } from 'react-dnd'
import { getEmptyImage } from 'react-dnd-html5-backend'

const columnSource = {
  beginDrag (props) {
    return { index: props.index, column: props.column }
  }
}

function collect (connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview()
  }
}

class DragColumn extends PureComponent {
  static propTypes = {
    index: PropTypes.number.isRequired, // eslint-disable-line
    connectDragSource: PropTypes.func.isRequired,
    connectDragPreview: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired
  }

  componentDidMount () {
    // Use empty image as a drag preview so browsers don't draw it
    // and we can draw whatever we want on the custom drag layer instead.
    this.props.connectDragPreview(getEmptyImage(), {
      // IE fallback: specify that we'd rather screenshot the node
      // when it already knows it's being dragged so we can hide it with CSS.
      captureDraggingState: true
    })
  }

  render () {
    const { connectDragSource, children } = this.props

    return connectDragSource(
      <div style={{ cursor: 'move' }}>
        {children}
      </div>
    )
  }
}

export default DragSource(DragTypes.COLUMN, columnSource, collect)(DragColumn)
