import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { DragLayer } from 'react-dnd'
import Column from '@/components/Column'

function collect (monitor) {
  return {
    item: monitor.getItem(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging()
  }
}

function getItemStyles (props) {
  const { currentOffset } = props
  if (!currentOffset) {
    return {
      display: 'none'
    }
  }

  const { x, y } = currentOffset
  const transform = `translate(${x}px, ${y}px)`
  return {
    transform: transform,
    WebkitTransform: transform
  }
}

class DragColumnLayer extends PureComponent {
  static propTypes = {
    item: PropTypes.object,
    currentOffset: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired
    }),
    isDragging: PropTypes.bool.isRequired
  }

  render () {
    const { item, isDragging } = this.props
    if (!isDragging) {
      return null
    }
    return (
      <Layer>
        <div style={getItemStyles(this.props)}>
          <Column number={item.index + 1} {...item.column} />
        </div>
      </Layer>
    )
  }
}

export default DragLayer(collect)(DragColumnLayer)

const Layer = styled.div`
  position: fixed;
  pointer-events: none;
  z-index: 100;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
`
