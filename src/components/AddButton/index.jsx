import React from 'react'
import styled from 'styled-components'
import addIcon from './assets/add.svg'

const AddButton = (props) => (
  <StyledButton {...props}>
    <Icon src={addIcon} />
  </StyledButton>
)

export default AddButton

const StyledButton = styled.button`
  cursor: pointer;
  width: 21px;
  height: 21px;
  background-color: transparent;
  border: none;
  outline: none;
  padding: 3px;
  margin: 0;
  border-radius: 50%;

  &:active {
    background-color: rgba(0, 0, 0, 0.1);
  }
`

const Icon = styled.img`
  width: 15px;
  height: 15px;
`
