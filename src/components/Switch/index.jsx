import React from 'react'
import PropTypes from 'prop-types'
import ReactSwitch from 'react-switch'
import '@/components/Switch/style.css'

const Switch = (props) => (
  <ReactSwitch
    {...props}
    uncheckedIcon={false}
    checkedIcon={false}
    height={20}
    width={38}
    boxShadow='0px 1px 5px rgba(0, 0, 0, 0.6)'
    activeBoxShadow='0px 0px 1px 10px rgba(0, 0, 0, 0.2)'
    offColor='#DCDCDC'
    onColor='#69F199'
    offHandleColor='#DCDCDC'
    onHandleColor='#3AF490'
  />
)

Switch.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired
}

export default Switch
