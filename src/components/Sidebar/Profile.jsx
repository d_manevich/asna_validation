import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import arrowDownIcon from './assets/arrow-down.svg'
import roleIcon from './assets/role.svg'

const Profile = ({ email, role }) => (
  <Wrapper>
    <div>Профиль</div>
    <Email>{email}</Email>
    <Role>{role}</Role>
  </Wrapper>
)

Profile.propTypes = {
  email: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired
}

export default Profile

const Wrapper = styled.div`
  width: 100%;
  font-size: 16px;
  color: white;
  font-weight: 400;
`

const Email = styled.div`
  position: relative;
  display: inline-block;
  font-size: 16px;
  font-weight: 600;
  line-height: 20px;
  padding-right: 20px;
  margin-top: 4px;

  &:after {
    content: '';
    position: absolute;
    right: 0;
    height: 100%;
    width: 11px;
    background-position: right center;
    background-repeat: no-repeat;
    background-size: 100% auto;
    background-image: url(${arrowDownIcon});
  }
`

const Role = styled.div`
  position: relative;
  display: inline-block;
  font-weight: 600;
  font-size: 16px;
  padding-left: 23px;
  margin-top: 2px;

  &:before {
    content: '';
    position: absolute;
    left: 0;
    height: 100%;
    width: 16px;
    background-position: left center;
    background-repeat: no-repeat;
    background-size: 100% auto;
    background-image: url(${roleIcon});
  }
`
