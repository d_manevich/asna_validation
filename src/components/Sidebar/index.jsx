import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import Select from '@/components/Select'
import Profile from '@/components/Sidebar/Profile'
import logo from './assets/logo.png'

const Sidebar = ({ pharmacyName, profileEmail, profileRole }) => (
  <Wrapper>
    <Logo src={logo} />
    <Pharmacy>Аптека:</Pharmacy>
    <PharmacyName>{pharmacyName}</PharmacyName>
    <StyledNavLink to='/loader'>Загрузить на проверку</StyledNavLink>
    <StyledNavLink to='/templates'>Шаблоны критериев</StyledNavLink>
    <StyledNavLink to='/logs'>Логирование</StyledNavLink>
    <Ftp>Выберите FTP-сервер:</Ftp>
    <StyledSelect
      value='asna'
      options={[{ label: 'ООО «АСНА»', value: 'asna' }]}
      onChange={() => { }}
      menuPlacement='auto'
      white
      small
    />
    <Profile email={profileEmail} role={profileRole} />
  </Wrapper>
)

Sidebar.propTypes = {
  pharmacyName: PropTypes.string.isRequired,
  profileEmail: PropTypes.string.isRequired,
  profileRole: PropTypes.string.isRequired
}

export default Sidebar

const Wrapper = styled.div`
  width: 315px;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  background-image: linear-gradient(170deg, #25b3b3, #60cb8c);
  padding: 35px 47px 43px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  color: white;
  font-weight: 400;
`

const Logo = styled.img`
  width: 100%;
  margin-bottom: 10px;
`

const Pharmacy = styled.div`
  font-size: 18px;
`

const PharmacyName = styled.div`
  font-size: 16px;
`

const StyledNavLink = styled(NavLink)`
  color: white;
  text-decoration: none;
  font-size: 16px;
  font-weight: 600;
  height: 30px;
  line-height: 28px;
  margin-top: 28px;

  &.active {
    border-bottom: 2px solid white;
  }
`

const Ftp = styled.div`
  margin-top: auto;
  font-size: 16px;
  color: white;
`

const StyledSelect = styled(Select)`
  margin-top: 8px;
  margin-bottom: 35px;
  width: 100%;
`
