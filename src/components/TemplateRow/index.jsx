import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import DeleteTemplateModal from '@/components/DeleteTemplateModal'

export default class TemplateRow extends PureComponent {
  static propTypes = {
    template: PropTypes.shape({
      name: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      active: PropTypes.bool.isRequired
    }).isRequired,
    onEnable: PropTypes.func.isRequired,
    onDisable: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
  }

  state = {
    isModalOpen: false
  }

  handleOpenModal = () => {
    this.setState({ isModalOpen: true })
  }

  handleCloseModal = () => {
    this.setState({ isModalOpen: false })
  }

  render () {
    const { onEnable, onDisable, onDelete } = this.props
    const { name, active, id } = this.props.template
    const { isModalOpen } = this.state

    return (
      <Wrapper active={active}>
        <Title active={active}>{name}</Title>
        <LinkWrapper active={active}>
          <Link to={`/templates/${id}`}>Просмотреть/изменить</Link>
        </LinkWrapper>
        <Del active={active} onClick={this.handleOpenModal}>Удалить</Del>
        {active &&
          <Toggle active onClick={onDisable}>Деактивировать</Toggle>
        }
        {!active &&
          <Toggle onClick={onEnable}>Активировать</Toggle>
        }
        <DeleteTemplateModal
          name={name}
          isOpen={isModalOpen}
          onClose={this.handleCloseModal}
          onDelete={onDelete}
        />
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  width: 100%;
  height: 50px;
  padding: 0 15px 0 25px;
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  border-radius: 3px;
  background-color: #F2F2F2;
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;
  background-image: ${props => props.active ? 'linear-gradient(135deg, #60CB8C, #25B3B3)' : 'none'};
`

const Title = styled.div`
  color: ${props => props.active ? 'white' : 'black'};
  margin-right: 30px;
  min-width: 50px;
`

const LinkWrapper = styled.span`
  color: ${props => props.active ? 'white' : '#5E5E5E'};
`

const Link = styled(NavLink)`
  text-decoration: none;
  color: inherit;
  opacity: 0.5;
`

const Del = styled.button`
  cursor: pointer;
  margin-left: auto;
  margin-right: 20px;
  font-size: 16px;
  padding: 0;
  border: none;
  outline: none;
  background: transparent;
  color: ${props => props.active ? 'white' : '#5E5E5E'};
  opacity: 0.5;
`

const Toggle = styled.button`
  cursor: pointer;
  text-align: left;
  width: 130px;
  font-size: 16px;
  font-weight: 600;
  padding: 0;
  border: none;
  outline: none;
  background: transparent;
  color: ${props => props.active ? 'white' : 'black'};
`
