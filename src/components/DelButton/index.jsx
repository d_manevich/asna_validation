import React from 'react'
import styled from 'styled-components'
import delIcon from './assets/del.svg'

const DelButton = (props) => (
  <StyledButton {...props}>
    <Icon src={delIcon} />
  </StyledButton>
)

export default DelButton

const StyledButton = styled.button`
  cursor: pointer;
  width: 21px;
  height: 21px;
  background-color: transparent;
  border: none;
  outline: none;
  padding: 3px;
  margin: 0;
  border-radius: 50%;

  &:active {
    background-color: rgba(0, 0, 0, 0.1);
  }
`

const Icon = styled.img`
  width: 15px;
  height: 15px;
`
