import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import checkIcon from './assets/check.svg'

const OptionalCheckbox = ({ checked, onChange, className }) => (
  <Label className={className}>
    <StyledCheckbox>
      <CheckIcon src={checkIcon} checked={checked} />
      <input
        type='checkbox'
        checked={checked}
        onChange={() => onChange(!checked)}
      />
    </StyledCheckbox>
    Может быть пустым
  </Label>
)

OptionalCheckbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string
}

export default OptionalCheckbox

const Label = styled.label`
  cursor: pointer;
  position: relative;
  display: inline-block;
  font-size: 16px;
  color: #4ED17F;
  width: 150px;
  height: 36px;
  padding-left: 48px;
`

const StyledCheckbox = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 36px;
  height: 36px;
  border-radius: 3px;
  background-color: rgba(78, 209, 127, 0.4);

  input {
    display: none;
  }
`

const CheckIcon = styled.img`
  visibility: ${props => props.checked ? 'visible' : 'hidden'};
  position: absolute;
  width: 24px;
  height: 17px;
  top: 9px;
  left: 7px;
`
