import React, { PureComponent } from 'react'
import { find } from 'ramda'
import PropTypes from 'prop-types'
import ReactSelect from 'react-select'

export default class Select extends PureComponent {
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    options: PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
      label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
    })).isRequired,
    onChange: PropTypes.func.isRequired,
    small: PropTypes.bool,
    white: PropTypes.bool,
    disabled: PropTypes.bool
  }

  static defaultProps = {
    small: false,
    white: false,
    disabled: false
  }

  get selectedOption () {
    const { options, value } = this.props
    return find(option => option.value === value, options)
  }

  handleChange = (option) => {
    this.props.onChange(option && option.value)
  }

  render () {
    const { options, value, onChange, small, white, disabled, ...props } = this.props
    return (
      <ReactSelect
        value={this.selectedOption}
        options={options}
        onChange={this.handleChange}
        backspaceRemovesValue={false}
        styles={customStyles(small, white)}
        isDisabled={disabled}
        {...props}
      />
    )
  }
}

const customStyles = (isSmall, isWhite) => ({
  control: base => ({
    ...base,
    'border-radius': '3px',
    'border-color': isWhite ? 'white' : 'black',
    'box-shadow': 'none',
    'background-color': 'transparent',
    'min-height': isSmall ? '28px' : '36px',
    'max-height': isSmall ? '28px' : '36px',
    'font-size': '16px',
    '&:hover': {
      'border-color': isWhite ? 'white' : 'black'
    }
  }),
  indicatorSeparator: () => ({
    display: 'none'
  }),
  placeholder: base => ({
    ...base,
    color: isWhite ? 'white' : 'black',
    top: isSmall ? '4px' : '7px',
    transform: 'none',
    'margin-left': 0
  }),
  input: base => ({
    ...base,
    color: isWhite ? 'white' : 'black'
  }),
  singleValue: base => ({
    ...base,
    color: isWhite ? 'white' : 'black',
    top: isSmall ? '4px' : '7px',
    transform: 'none',
    'margin-left': 0
  }),
  valueContainer: base => ({
    ...base,
    padding: isSmall ? '0 8px 0 9px' : '2px 8px 2px 20px'
  }),
  dropdownIndicator: base => ({
    ...base,
    padding: isSmall ? '4px' : '8px'
  })
})
