import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Select from '@/components/Select'

export default class ExampleSelect extends Component {
  static propTypes = {
    small: PropTypes.bool,
    white: PropTypes.bool,
    disabled: PropTypes.bool
  }

  static defaultProps = {
    small: false,
    white: false,
    disabled: false
  }

  state = {
    value: null
  }

  get options () {
    return [
      { label: 'First', value: 'first' },
      { label: 'Second', value: 'second' }
    ]
  }

  handleChange = value => {
    this.setState({ value })
  }

  render () {
    const { small, white, disabled } = this.props
    return (
      <Select
        value={this.state.value}
        options={this.options}
        onChange={this.handleChange}
        small={small}
        white={white}
        disabled={disabled}
      />
    )
  }
}
