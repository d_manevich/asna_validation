import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Button = ({ children, ...props }) => (
  <StyledButton {...props} >{children}</StyledButton>
)

Button.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.oneOf(['sm', 'md', 'lg', 'xlg']),
  color: PropTypes.oneOf(['default', 'primary', 'secondary'])
}

Button.defaultProps = {
  size: 'sm',
  color: 'default'
}

export default Button

const SIZES = {
  sm: 36,
  md: 40,
  lg: 50,
  xlg: 60
}

const FONT_SIZES = {
  sm: 16,
  md: 16,
  lg: 16,
  xlg: 22
}

const COLORS = {
  default: 'black',
  primary: 'white',
  secondary: '#4ED17F'
}

const BORDERS = {
  default: '1px solid black',
  primary: 'none',
  secondary: '1px solid #4ED17F'
}

const BACKGROUND_IMAGES = {
  default: 'none',
  primary: 'linear-gradient(135deg, #60CB8C, #25B3B3)',
  secondary: 'none'
}

const StyledButton = styled.button`
  cursor: pointer;
  background-color: transparent;
  font-weight: 400;
  padding: 0 60px;
  height: ${props => SIZES[props.size]}px;
  font-size: ${props => FONT_SIZES[props.size]}px;
  color: ${props => COLORS[props.color]};
  border: ${props => BORDERS[props.color]};
  outline: none;
  border-radius: 3px;
  background-image: ${props => BACKGROUND_IMAGES[props.color]};
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;

  &[disabled] {
    cursor: not-allowed;
    opacity: 0.6;
  }
`
