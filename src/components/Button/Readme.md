```js
<Button>Push Me</Button>
```

Types:

```jsx
<Button color="default" style={{margin: '20px'}}>
  default
</Button>
<Button color="primary" style={{margin: '20px'}}>
  primary
</Button>
<Button color="secondary" style={{margin: '20px'}}>
  secondary
</Button>
```

Sizes:

```jsx
<Button size="sm" color="primary" style={{margin: '20px'}}>
  sm
</Button>
<Button size="md" color="primary" style={{margin: '20px'}}>
  md
</Button>
<Button size="lg" color="primary" style={{margin: '20px'}}>
  lg
</Button>
<Button size="xlg" color="primary" style={{margin: '20px'}}>
  xlg
</Button>
```