import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const LoaderRow = ({ name, progress, type }) => (
  <Wrapper type={type} progress={progress}>
    <span>{name}</span>
    <span>{progress}%</span>
  </Wrapper>
)

LoaderRow.propTypes = {
  name: PropTypes.string.isRequired,
  progress: PropTypes.number.isRequired,
  type: PropTypes.oneOf(['upload', 'processing']).isRequired
}

export default LoaderRow

const BACKGROUND_IMAGES = {
  upload: 'linear-gradient(135deg, #60CB8C, #25B3B3)',
  processing: 'linear-gradient(135deg, #1EB9C7, #3876BE)'
}

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 50px;
  background-color: white;
  border-radius: 3px;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
  padding: 0 10px 0 25px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  overflow: hidden;
  margin-bottom: 10px;

  span {
    position: relative;
  }

  &:before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: ${props => props.progress}%;
    background-image: ${props => BACKGROUND_IMAGES[props.type]};
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100%;
  }
`
