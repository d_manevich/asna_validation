import { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { createPortal } from 'react-dom'

const body = document.body
const appRoot = document.getElementById('root')

export default class Portal extends PureComponent {
  static propTypes = {
    onClose: PropTypes.func,
    closeOnEsc: PropTypes.bool,
    className: PropTypes.string,
    children: PropTypes.node
  }

  static defaultProps = {
    closeOnEsc: false
  }

  constructor (props) {
    super(props)
    this.el = document.createElement('div')
  }

  addKeyListener (closeOnEsc) {
    window.removeEventListener('keydown', this.handleKeyDown)
    if (closeOnEsc) window.addEventListener('keydown', this.handleKeyDown)
  }

  handleKeyDown = event => {
    const { onClose } = this.props
    if (event.keyCode === 27 && onClose) onClose()
  }

  componentDidMount () {
    body.insertBefore(this.el, appRoot)
    this.addKeyListener(this.props.closeOnEsc)
  }

  componentWillUnmount () {
    body.removeChild(this.el)
    window.removeEventListener('keydown', this.handleKeyDown)
  }

  render () {
    const { className, children } = this.props

    if (className) this.el.classList.add(...className.split(' '))
    return createPortal(children, this.el)
  }
}
