import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Portal from '@/components/Modal/Portal'

export default class Modal extends PureComponent {
  static propTypes = {
    onClose: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    children: PropTypes.node
  }

  render () {
    const { isOpen, children, onClose } = this.props

    if (!isOpen) return null

    return (
      <StyledPortal closeOnEsc onClose={onClose}>
        <Overlay onClick={onClose} />
        {children}
      </StyledPortal>
    )
  }
}

const StyledPortal = styled(Portal)`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 315px;
  right: 0;
  overflow: hidden;
  z-index: 1000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const Overlay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background: rgba(101, 101, 101, 0.9);
  cursor: pointer;
`
