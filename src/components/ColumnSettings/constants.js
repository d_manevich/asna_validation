export const formOptions = [
  { label: 'Математическое сравнение', value: 'compare' },
  { label: 'Тип столбца', value: 'columnType' },
  { label: 'Регулярное выражение', value: 'regExp' },
  { label: 'Длина символов', value: 'length' },
  { label: 'Соответствие списку', value: 'list' },
  { label: 'Условная проверка', value: 'conditional' }
]

export const conditionalFormOptions = formOptions.filter(option => option.value !== 'conditional')

export const initialCondition = {
  column: null,
  if: { type: null, model: null },
  then: { type: null, model: null }
}

export const formModels = {
  compare: {
    value: '',
    type: 'moreThan'
  },
  columnType: {
    value: 'string'
  },
  regExp: {
    value: ''
  },
  length: {
    value: ''
  },
  list: {
    value: ['']
  },
  conditional: {
    value: [
      initialCondition,
      {
        then: { type: null, model: null }
      }
    ]
  }
}
