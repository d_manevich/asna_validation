import React, { PureComponent } from 'react'
import { update, remove } from 'ramda'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Select from '@/components/Select'
import Button from '@/components/Button'
import OptionalCheckbox from '@/components/OptionalCheckbox'
import Form from '@/components/ColumnSettings/forms'

import { formOptions, formModels } from '@/components/ColumnSettings/constants'

export default class ColumnSettings extends PureComponent {
  static propTypes = {
    number: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    requirements: PropTypes.arrayOf(PropTypes.shape({
      type: PropTypes.string,
      model: PropTypes.object
    })).isRequired,
    columns: PropTypes.array.isRequired,
    optional: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    onToggleOptional: PropTypes.func.isRequired
  }

  handleSelectNew = type => {
    const model = formModels[type]
    this.props.onChange([{ type, model }])
  }

  handleEdit = (index, model) => {
    const { requirements, onChange } = this.props
    onChange(update(index, model, requirements))
  }

  handleDel = index => {
    const { requirements, onChange } = this.props
    onChange(remove(index, 1, requirements))
  }

  handleChangeType = (index, type) => {
    const { requirements, onChange } = this.props
    const model = formModels[type]
    onChange(update(index, { type, model }, requirements))
  }

  handleAddNew = () => {
    const { requirements, onChange } = this.props
    const model = formModels.compare
    onChange([ ...requirements, { type: 'compare', model } ])
  }

  render () {
    const { number, name, requirements, columns, optional, onToggleOptional } = this.props
    const hasRequirements = !!requirements.length
    const canDel = requirements.length > 1

    return (
      <Wrapper>
        <Describe>
          <Number>{number}</Number>
          <div>{name}</div>
        </Describe>
        <Container>
          {!hasRequirements &&
            <Requirement initial>
              <StyledSelect
                value='null'
                options={formOptions}
                placeholder='Выберите условие'
                onChange={this.handleSelectNew}
              />
            </Requirement>
          }
          {requirements.map((requirement, index) => (
            <Requirement key={index}>
              <Form
                type={requirement.type}
                model={requirement.model}
                name={name}
                columns={columns}
                forms={formOptions}
                canDel={canDel}
                onEdit={model => this.handleEdit(index, model)}
                onDel={() => this.handleDel(index)}
                onChangeType={type => this.handleChangeType(index, type)}
              />
            </Requirement>
          ))}
          {hasRequirements &&
            <Bottom>
              <AddRequirement
                color='secondary'
                onClick={this.handleAddNew}
              >
                Добавить ещё условие
              </AddRequirement>
              <Optional checked={optional} onChange={onToggleOptional} />
            </Bottom>
          }
        </Container>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  background-color: white;
  padding: 25px 40px 35px 20px;
  border-radius: 3px;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
  display: flex;
  align-items: flex-start;
  width: 100%;

  & + & {
    margin-top: 20px;
  }
`

const Describe = styled.div`
  width: 135px;
  padding-right: 30px;
`

const Number = styled.div`
  font-size: 36px;
`

const Container = styled.div`
  width: 100%;
  position: relative;
`

const Bottom = styled.div`
  display: flex;
  align-items: center;
  margin-top: 60px;
`

const Requirement = styled.div`
  padding-left: 20px;
  border-left: 1px solid #979797;
  padding-bottom: ${props => props.initial ? '25px' : '0'};

  &:first-child {
    padding-top: 40px;
  }

  & + & {
    padding-top: 30px;
  }
`

const StyledSelect = styled(Select)`
  width: 310px;
`

const AddRequirement = styled(Button)`
  margin-left: 20px;
  padding: 0 40px 0 20px;
`

const Optional = styled(OptionalCheckbox)`
  margin-left: auto;
`
