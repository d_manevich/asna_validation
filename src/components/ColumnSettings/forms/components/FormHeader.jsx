import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import DelButton from '@/components/DelButton'

const FormHeader = ({ canDel, onDel, children, ...props }) => (
  <Wrapper canDel={canDel} {...props}>
    {children}
    {canDel && <StyledDel onClick={onDel} />}
  </Wrapper>
)

FormHeader.propTypes = {
  canDel: PropTypes.bool.isRequired,
  onDel: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired
}

export default FormHeader

const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  padding-right: ${props => props.canDel ? '30px' : '0'};
  width: 100%;
  padding-bottom: 10px;
`

const StyledDel = styled(DelButton)`
  position: absolute;
  top: 8px;
  right: 0;
`
