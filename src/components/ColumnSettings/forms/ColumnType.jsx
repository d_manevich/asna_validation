import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Select from '@/components/Select'
import FormHeader from './components/FormHeader'
import Choose from './Choose'

const TYPE_OPTIONS = [
  { value: 'string', label: 'Строковый' },
  { value: 'integer', label: 'Целочисленный' },
  { value: 'date', label: 'Дата и время' },
  { value: 'decimal', label: 'Десятичное число' },
  { value: 'monetary', label: 'Денежный' }
]

export default class ColumnType extends PureComponent {
  static propTypes = {
    type: PropTypes.string.isRequired,
    model: PropTypes.object.isRequired,
    forms: PropTypes.array.isRequired,
    canDel: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDel: PropTypes.func.isRequired
  }

  handleChange = value => {
    const { model, onEdit } = this.props
    onEdit({type: 'columnType', model: { ...model, value }})
  }

  render () {
    const { model, canDel, onDel } = this.props

    return (
      <FormHeader canDel={canDel} onDel={onDel}>
        <Choose {...this.props} />
        <StyledSelect
          options={TYPE_OPTIONS}
          value={model.value}
          onChange={this.handleChange}
        />
      </FormHeader>
    )
  }
}

const StyledSelect = styled(Select)`
  flex-grow: 1;
  max-width: 310px;
  margin-left: 10px;
`
