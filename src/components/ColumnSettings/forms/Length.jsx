import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Input from '@/components/Input'
import FormHeader from './components/FormHeader'
import Choose from './Choose'

export default class Length extends PureComponent {
  static propTypes = {
    type: PropTypes.string.isRequired,
    model: PropTypes.object.isRequired,
    forms: PropTypes.array.isRequired,
    canDel: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDel: PropTypes.func.isRequired
  }

  handleEdit = event => {
    const { model, onEdit } = this.props
    onEdit({type: 'length', model: { ...model, value: event.target.value }})
  }

  render () {
    const { canDel, onDel, model } = this.props

    return (
      <Wrapper>
        <FormHeader canDel={canDel} onDel={onDel}>
          <Choose {...this.props} />
          <StyledInput
            value={model.value}
            onChange={this.handleEdit}
            placeholder='Введите число'
          />
        </FormHeader>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
  display: flex;
`

const StyledInput = styled(Input)`
  margin-left: 10px;
  max-width: 230px;
  flex-grow: 1;
`
