import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Select from '@/components/Select'
import { formModels } from '@/components/ColumnSettings/constants'

export default class Choose extends PureComponent {
  static propTypes = {
    type: PropTypes.string,
    forms: PropTypes.array.isRequired,
    onEdit: PropTypes.func.isRequired
  }

  handleChangeType = type => {
    this.props.onEdit({type, model: formModels[type]})
  }

  render () {
    const { type, forms } = this.props
    return (
      <StyledSelect
        value={type}
        options={forms}
        onChange={this.handleChangeType}
        placeholder='Выберите условие'
      />
    )
  }
}

const StyledSelect = styled(Select)`
  max-width: 310px;
  min-width: 120px;
  flex-grow: 1;
`
