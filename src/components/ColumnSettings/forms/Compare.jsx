import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Select from '@/components/Select'
import Input from '@/components/Input'
import FormHeader from './components/FormHeader'
import Choose from './Choose'

const COMPARE_OPTIONS = [
  { value: 'moreThan', label: 'Больше чем' },
  { value: 'lessThan', label: 'Меньше чем' },
  { value: 'equally', label: 'Равно' },
  { value: 'moreOrEqual', label: 'Больше или равно' },
  { value: 'lessOrEqual', label: 'Меньше или равно' }
]

export default class Compare extends PureComponent {
  static propTypes = {
    type: PropTypes.string.isRequired,
    model: PropTypes.object.isRequired,
    forms: PropTypes.array.isRequired,
    canDel: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDel: PropTypes.func.isRequired
  }

  handleEditValue = event => {
    const { model, onEdit } = this.props
    onEdit({type: 'compare', model: { ...model, value: event.target.value }})
  }

  handleEditType = value => {
    const { model, onEdit } = this.props
    onEdit({type: 'compare', model: { ...model, type: value }})
  }

  render () {
    const { model, canDel, onDel } = this.props

    return (
      <Wrapper>
        <FormHeader canDel={canDel} onDel={onDel}>
          <Choose {...this.props} />
        </FormHeader>
        <Inputs>
          <ValueSelect
            options={COMPARE_OPTIONS}
            value={model.type}
            onChange={this.handleEditType}
          />
          <ValueInput
            placeholder='Введите число'
            value={model.value}
            onChange={this.handleEditValue}
          />
        </Inputs>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
`

const Inputs = styled.div`
  display: flex;
`

const ValueSelect = styled(Select)`
  max-width: 160px;
  min-width: 100px;
  margin-right: 10px;
  flex-grow: 1;
`

const ValueInput = styled(Input)`
  max-width: 230px;
  flex-grow: 1;
`
