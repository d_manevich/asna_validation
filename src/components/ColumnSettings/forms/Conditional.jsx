import React, { PureComponent } from 'react'
import { update, init, last, remove, length, insert } from 'ramda'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Select from '@/components/Select'
import Button from '@/components/Button'
import DelConditionBtn from '@/components/DelConditionBtn'
import Form from '@/components/ColumnSettings/forms'
import FormHeader from './components/FormHeader'
import Choose from './Choose'
import { conditionalFormOptions, initialCondition } from '@/components/ColumnSettings/constants'

export default class Conditional extends PureComponent {
  static propTypes = {
    type: PropTypes.string.isRequired,
    model: PropTypes.object.isRequired,
    forms: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    columns: PropTypes.array.isRequired,
    canDel: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDel: PropTypes.func.isRequired
  }

  handleEdit = model => {
    this.props.onEdit({ type: 'conditional', model })
  }

  handleColumnChange = (index, value) => {
    const { model } = this.props
    const condition = model.value[index]
    this.handleEdit({ value: update(index, { ...condition, column: value }, model.value) })
  }

  handleAdd = () => {
    const { model } = this.props
    this.handleEdit({ value: insert(length(model.value) - 1, initialCondition, model.value) })
  }

  handleDel = index => {
    const { model } = this.props
    this.handleEdit({ value: remove(index, 1, model.value) })
  }

  handleConditionEdit = (index, statement, conditionalModel) => {
    const { model } = this.props
    const condition = model.value[index]
    this.handleEdit({ value: update(index, { ...condition, [statement]: conditionalModel }, model.value) })
  }

  get columnsOptions () {
    const { columns, name } = this.props
    return columns.filter(column => column !== name).map(column => ({ label: column, value: column }))
  }

  render () {
    const { canDel, onDel, model, name } = this.props
    const conditionals = init(model.value)
    const elseConditionIndex = length(model.value) - 1
    const elseCondition = last(model.value) //eslint-disable-line
    const canDelCondition = conditionals.length > 1

    return (
      <Wrapper>
        <StyledFormHeader canDel={canDel} onDel={onDel}>
          <Choose {...this.props} />
        </StyledFormHeader>
        {conditionals.map((condition, index) => (
          <Condition key={index}>
            <Statement>
              <Header>
                <Title>Если</Title>
                <ColumnSelect
                  value={condition.column}
                  options={this.columnsOptions}
                  onChange={value => this.handleColumnChange(index, value)}
                  placeholder='Выберите столбец'
                />
              </Header>
              <Form
                type={condition.if.type}
                model={condition.if.model}
                forms={conditionalFormOptions}
                onEdit={model => this.handleConditionEdit(index, 'if', model)}
              />
            </Statement>
            <Statement>
              <Header>
                <Title>To</Title><Name>{name}</Name>
              </Header>
              <Form
                type={condition.then.type}
                model={condition.then.model}
                forms={conditionalFormOptions}
                onEdit={model => this.handleConditionEdit(index, 'then', model)}
              />
            </Statement>
            {canDelCondition &&
              <StyledDelCondition onClick={() => this.handleDel(index)}>
                Удалить это условие
              </StyledDelCondition>
            }
          </Condition>
        ))}
        <AddButton color='secondary' onClick={this.handleAdd}>Добавить ещё условную проверку</AddButton>
        <Else>
          <ElseHeader>Если условие не выполняется, то</ElseHeader>
          <ElseTitle>{name}</ElseTitle>
          <Form
            type={elseCondition.then.type}
            model={elseCondition.then.model}
            forms={conditionalFormOptions}
            onEdit={model => this.handleConditionEdit(elseConditionIndex, 'then', model)}
          />
        </Else>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
`

const StyledFormHeader = styled(FormHeader)`
  margin-bottom: 10px;
`

const Condition = styled.div`
  position: relative;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  padding: 30px 0 60px;

  & + & {
    border-top: 1px solid rgba(151, 151, 151, 0.4);
  }
}`

const Statement = styled.div`
  width: calc(50% - 22px);
  flex-shrink: 0;

  & + & {
    margin-left: 44px;
  }
`

const Header = styled.div`
  display: flex;
  height: 36px;
  align-items: center;
  margin-bottom: 10px;
`

const Title = styled.span`
  font-weight: 700;
`

const ColumnSelect = styled(Select)`
  margin-left: 10px;
  flex-grow: 1;
`

const Name = styled.span`
  margin-left: 15px;
`

const AddButton = styled(Button)`
  margin-top: 20px;
`

const Else = styled.div`
  margin-top: 40px;
`

const ElseHeader = styled.div`
  padding-bottom: 14px;
  margin-bottom: 10px;
  border-bottom: 1px solid #979797;
`

const ElseTitle = styled.div`
  display: inline-flex;
  line-height: 36px;
  margin-right: 20px;
`

const StyledDelCondition = styled(DelConditionBtn)`
  position: absolute;
  right: 0;
  bottom: 10px;
`
