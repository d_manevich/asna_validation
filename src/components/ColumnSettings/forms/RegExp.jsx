import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Textarea from '@/components/Textarea'
import FormHeader from './components/FormHeader'
import Choose from './Choose'

export default class RegExp extends PureComponent {
  static propTypes = {
    type: PropTypes.string.isRequired,
    model: PropTypes.object.isRequired,
    forms: PropTypes.array.isRequired,
    canDel: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDel: PropTypes.func.isRequired
  }

  handleEdit = event => {
    const { model, onEdit } = this.props
    onEdit({type: 'regExp', model: { ...model, value: event.target.value }})
  }

  render () {
    const { canDel, onDel, model } = this.props

    return (
      <Wrapper>
        <FormHeader canDel={canDel} onDel={onDel}>
          <Choose {...this.props} />
        </FormHeader>
        <StyledTextarea
          value={model.value}
          onChange={this.handleEdit}
          placeholder='Введите текст'
        />
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
`

const StyledTextarea = styled(Textarea)`
  max-width: 600px;
  margin-top: 10px;
`
