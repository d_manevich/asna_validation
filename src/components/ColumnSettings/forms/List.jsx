import React, { PureComponent } from 'react'
import { update, remove, append } from 'ramda'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import DelButton from '@/components/DelButton'
import AddButton from '@/components/AddButton'
import Input from '@/components/Input'
import FormHeader from './components/FormHeader'
import Choose from './Choose'

export default class List extends PureComponent {
  static propTypes = {
    type: PropTypes.string.isRequired,
    model: PropTypes.object.isRequired,
    forms: PropTypes.array.isRequired,
    canDel: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDel: PropTypes.func.isRequired
  }

  handleEdit = model => {
    this.props.onEdit({ type: 'list', model })
  }

  handleEditList = (event, index) => {
    const { model } = this.props
    this.handleEdit({ ...model, value: update(index, event.target.value, model.value) })
  }

  handleDel = index => {
    const { model } = this.props
    this.handleEdit({ ...model, value: remove(index, 1, model.value) })
  }

  handleAdd = () => {
    const { model } = this.props
    this.handleEdit({ ...model, value: append('', model.value) })
  }

  render () {
    const { canDel, onDel, model } = this.props
    const list = model.value
    const lastIndex = list.length - 1

    return (
      <Wrapper>
        <FormHeader canDel={canDel} onDel={onDel}>
          <Choose {...this.props} />
        </FormHeader>
        <ListWrapper>
          {list.map((value, index) => (
            <ListItem key={index}>
              <StyledInput
                value={value}
                onChange={event => this.handleEditList(event, index)}
                placeholder='Введите данные'
              />
              { lastIndex !== index && <DelButton onClick={() => this.handleDel(index)} /> }
              { lastIndex === index && <AddButton onClick={this.handleAdd} /> }
            </ListItem>
          ))}
        </ListWrapper>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
`

const ListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`

const ListItem = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
`

const StyledInput = styled(Input)`
  max-width: 350px;
  margin-right: 5px;
`
