import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import Compare from './Compare'
import ColumnType from './ColumnType'
import RegExp from './RegExp'
import Length from './Length'
import List from './List'
import Conditional from './Conditional'
import Choose from './Choose'

export default class Forms extends PureComponent {
  static propTypes = {
    type: PropTypes.string,
    canDel: PropTypes.bool,
    onDel: PropTypes.func
  }

  static defaultProps = {
    canDel: false,
    onDel: () => {}
  }

  render () {
    switch (this.props.type) {
      case 'compare':
        return <Compare {...this.props} />
      case 'columnType':
        return <ColumnType {...this.props} />
      case 'regExp':
        return <RegExp {...this.props} />
      case 'length':
        return <Length {...this.props} />
      case 'list':
        return <List {...this.props} />
      case 'conditional':
        return <Conditional {...this.props} />
      case null:
        return <Choose {...this.props} />
      default:
        return null
    }
  }
}
