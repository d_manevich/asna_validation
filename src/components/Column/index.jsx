import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import DelButton from '@/components/DelButton'
import Switch from '@/components/Switch'

export default class Column extends PureComponent {
  static propTypes = {
    number: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    onChange: PropTypes.func,
    onDelete: PropTypes.func
  }

  static defaultProps = {
    onChange: () => { },
    onDelete: () => { }
  }

  render () {
    const {
      number,
      name,
      active,
      onChange,
      onDelete
    } = this.props

    return (
      <Wrapper checked={active}>
        <DelWrapper>
          <DelButton onClick={onDelete} />
        </DelWrapper>
        <Number>{number}</Number>
        <Description>{name}</Description>
        <SwitchWrapper>
          <Switch checked={active} onChange={onChange} />
        </SwitchWrapper>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  height: 200px;
  width: 133px;
  padding: 10px 10px 17px;
  border-radius: 2px;
  background-color: ${props => props.checked ? '#C5FFE4' : '#F2F2F2'};
`

const DelWrapper = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
`

const Number = styled.div`
  font-size: 36px;
  color: black;
`

const Description = styled.div`
  font-size: 16px;
  color: black;
  margin-top: 5px;
`

const SwitchWrapper = styled.div`
  width: 100%;
  margin-top: auto;
  border-top: 1px solid black;
  padding-top: 10px;
  display: flex;
  justify-content: flex-end;
`
