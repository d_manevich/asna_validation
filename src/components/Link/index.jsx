import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

const Link = ({ type, ...props }) => (
  <StyledLink type={type} {...props} />
)

Link.propTypes = {
  type: PropTypes.oneOf(['default', 'button'])
}

Link.defaultProps = {
  type: 'default'
}

export default Link

const StyledLink = styled(NavLink)`
  display: inline-block;
  text-decoration: none;
  color: black;
  font-weight: 400;
  border-radius: 3px;
  padding: ${props => props.type === 'default' ? '0' : '0 20px'};
  line-height: ${props => props.type === 'default' ? '18px' : '38px'};
  border: ${props => props.type === 'default' ? 'none' : '1px solid #4ED17F'};
`
