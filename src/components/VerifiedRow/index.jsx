import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Checkbox from '@/components/Checkbox'
import Button from '@/components/Button'
import arrowIcon from './assets/arrow.svg'
import loadIcon from './assets/load.svg'

export default class VerifiedRow extends PureComponent {
  static propTypes = {
    checked: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
  }

  state = {
    isOpen: false
  }

  handleToggle = () => {
    this.setState(prevState => ({ isOpen: !prevState.isOpen }))
  }

  render () {
    const { valid, checked, name, onChange } = this.props
    const { isOpen } = this.state

    return (
      <Wrapper>
        <Header valid={valid} onClick={this.handleToggle}>
          {valid &&
            <StyledCheckbox checked={checked} onChange={onChange} />
          }
          {name}
          <Arrow src={arrowIcon} open={isOpen} />
        </Header>
        {isOpen &&
          <Info>
            <Table>
              <thead>
                <tr>
                  <Th>Критерии:</Th>
                  <Th>Ошибки:</Th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <Td>Сумма оптовая без НДС</Td>
                  <ErrorTd valid>0.02%</ErrorTd>
                </tr>
                <tr>
                  <Td>Сумма оптовая с НДС</Td>
                  <ErrorTd valid>23%</ErrorTd>
                </tr>
                <tr>
                  <Td>Сумма НДС</Td>
                  <ErrorTd valid>2%</ErrorTd>
                </tr>
                <tr>
                  <Td>Сумма розничная</Td>
                  <ErrorTd valid>0.02%</ErrorTd>
                </tr>
                <tr>
                  <Td>Сумма реализации</Td>
                  <ErrorTd valid>0.01%</ErrorTd>
                </tr>
              </tbody>
            </Table>
            <RightBlock>
              <Table>
                <thead>
                  <tr>
                    <Th>Действия:</Th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ActionBtn>Загрузить EXEL</ActionBtn>
                      <ActionBtn right>Полный отчёт</ActionBtn>
                    </td>
                  </tr>
                </tbody>
              </Table>
              {valid && <UploadBtn color='primary' size='md'>Загрузить в экосистему</UploadBtn>}
            </RightBlock>
          </Info>
        }
      </Wrapper>
    )
  }
}

const BACKGROUND_IMAGES = {
  true: 'linear-gradient(135deg, #60CB8C, #25B3B3)',
  false: 'linear-gradient(135deg, #FF6F6F, #DA2A2A)'
}

const Wrapper = styled.div`
  width: 100%;
  background-color: white;
  border-radius: 3px;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
  overflow: hidden;
  margin-bottom: 10px;
`

const Header = styled.div`
  cursor: pointer;
  position: relative;
  height: 50px;
  width: 100%;
  display: flex;
  align-items: center;
  box-shadow: 0 2px 4px 0 rgba(188,188,188,0.50);
  padding-left: 45px;
  background-image: ${props => BACKGROUND_IMAGES[props.valid]};
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;
`

const StyledCheckbox = styled(Checkbox)`
  position: absolute;
  left: 14px;
`

const Arrow = styled.img`
  position: absolute;
  right: 16px;
  width: 13px;
  height: 7px;
  transform: ${props => props.open ? 'rotate(180deg)' : 'none'};
`

const Info = styled.div`
  width: 100%;
  padding: 40px 40px 45px 45px;
  background-color: white;
  display: flex;
  justify-content: space-between;
`

const RightBlock = styled.div`
  display: flex;
  flex-direction: column;
`

const Table = styled.table`
  border-collapse: collapse;
`

const Th = styled.th`
  font-size: 16px;
  font-weight: 400;
  color: #9A9A9A;
  text-align: left;
`

const Td = styled.td`
  padding-top: 20px;
  padding-right: 50px;
  font-size: 16px;
  color: black;
  font-weight: 400;
`

const ErrorTd = Td.extend`
  color: ${props => props.valid ? '#4CD89B' : '#E70022'};
`

const ActionBtn = styled.div`
  position: relative;
  display: inline-block;
  width: 110px;
  padding-left: 22px;
  margin-top: 17px;
  margin-right: 35px;

  &:before {
    content: '';
    position: absolute;
    top: 4px;
    left: 0;
    width: 16px;
    height: 16px;
    background-image: url(${loadIcon});
    transform: ${props => props.right ? 'rotate(-90deg)' : 'none'};
  }
`

const UploadBtn = styled(Button)`
  margin-top: auto;
`
