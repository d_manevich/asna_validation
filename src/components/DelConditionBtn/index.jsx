import React from 'react'
import styled from 'styled-components'
import icon from './assets/del.svg'

const DelConditionBtn = (props) => <Btn {...props} />

export default DelConditionBtn

const Btn = styled.button`
  cursor: pointer;
  height: 20px;
  padding: 0 0 0 20px;
  background-color: transparent;
  border: none;
  outline: none;
  color: #D14E4E;
  font-size: 16px;
  background-image: url(${icon});
  background-repeat: no-repeat;
  background-position-y: 2px;
`
