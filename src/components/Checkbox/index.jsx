import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import checkIcon from './assets/check.svg'

const Checkbox = ({ checked, onChange, className }) => (
  <Label className={className}>
    <CheckIcon src={checkIcon} checked={checked} />
    <input
      type='checkbox'
      checked={checked}
      onChange={() => onChange(!checked)}
    />
  </Label>
)

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string
}

export default Checkbox

const Label = styled.label`
  cursor: pointer;
  position: relative;
  display: inline-block;
  width: 14px;
  height: 14px;
  border-radius: 1px;
  border: 1px solid black;

  input {
    display: none;
  }
`

const CheckIcon = styled.img`
  visibility: ${props => props.checked ? 'visible' : 'hidden'};
  position: absolute;
  width: 16px;
  height: 16px;
  top: -2px;
  left: -2px;
`
