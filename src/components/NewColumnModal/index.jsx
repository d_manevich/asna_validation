import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Modal from '@/components/Modal'
import Input from '@/components/Input'
import Button from '@/components/Button'

export default class NewColumnModal extends PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired
  }

  state = {
    name: ''
  }

  handleChange = event => {
    this.setState({ name: event.target.value })
  }

  handleSave = (event) => {
    event.preventDefault()
    const name = this.state.name.trim()

    if (!name) return

    this.props.onSave(name)
    this.setState({ name: '' })
    this.props.onClose()
  }

  render () {
    const { isOpen, onClose } = this.props
    const { name } = this.state
    return (
      <Modal isOpen={isOpen} onClose={onClose}>
        <Wrapper>
          <Title>Создание нового столбца</Title>
          <Form onSubmit={this.handleSave}>
            <Input
              autoFocus
              value={name}
              onChange={this.handleChange}
              placeholder='Название столбца'
            />
            <StyledButton color='primary' disabled={!name}>
              Сохранить
            </StyledButton>
          </Form>
        </Wrapper>
      </Modal>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
  width: 665px;
  padding: 50px 70px 55px 55px;
  border-radius: 3px;
  background-color: white;
  border: 1px solid #979797;
  box-shadow: 0 2px 10px 0 rgba(0,0,0,0.50);
`

const Title = styled.div`
  font-size: 30px;
  line-height: 34px;
  width: 250px;
  margin-bottom: 35px;
`

const Form = styled.form`
  width: 100%;
  display: flex;
  align-items: center;
`

const StyledButton = styled(Button)`
  margin-left: 10px;
  flex-shrink: 0;
`
