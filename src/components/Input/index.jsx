import React from 'react'
import styled from 'styled-components'

const Input = (props) => (
  <StyledInput {...props} />
)

export default Input

const StyledInput = styled.input`
  width: 100%;
  height: 36px;
  border: none;
  outline: none;
  padding: 0 10px;
  background-color: rgba(221, 221, 221, 0.4);
  border-radius: 3px;
  color: black;
  font-size: 16px;
  font-weight: 400;
`
