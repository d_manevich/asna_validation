import React from 'react'
import styled from 'styled-components'

const Textarea = (props) => (
  <StyledTextarea {...props} />
)

export default Textarea

const StyledTextarea = styled.textarea`
  width: 100%;
  height: 165px;
  border: none;
  outline: none;
  padding: 10px;
  background-color: rgba(221, 221, 221, 0.4);
  border-radius: 3px;
  color: black;
  font-size: 16px;
  font-weight: 400;
  resize: none;
`
