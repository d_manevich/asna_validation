import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Modal from '@/components/Modal'
import Button from '@/components/Button'

export default class NewColumnModal extends PureComponent {
  static propTypes = {
    name: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
  }

  handleDelete = () => {
    this.props.onDelete()
    this.props.onClose()
  }

  render () {
    const { isOpen, onClose, name } = this.props

    return (
      <Modal isOpen={isOpen} onClose={onClose}>
        <Wrapper>
          <Title>Удалить {name}?</Title>
          <Form>
            <StyledButton onClick={this.handleDelete}>Удалить</StyledButton>
            <StyledButton color='primary' onClick={onClose}>Не удалять</StyledButton>
          </Form>
        </Wrapper>
      </Modal>
    )
  }
}

const Wrapper = styled.div`
  position: relative;
  padding: 70px 95px 90px;
  border-radius: 3px;
  background-color: white;
  box-shadow: 0 2px 4px 0 rgba(101,101,101,0.50);
`

const Title = styled.div`
  font-size: 32px;
  margin-bottom: 35px;
`

const Form = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  margin: -5px;
`

const StyledButton = styled(Button)`
  margin: 0 5px;
  width: 230px;
`
