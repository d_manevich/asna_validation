small
```js
initialState = { value: "download" };
const tabs = [
  { "label": "Загрузка: 12", "value": "download" },
  { "label": "В обработке: 32", "value": "processing" },
  { "label": "Проверено: 6", "value": "checked" }
];
<Tabs
  tabs={tabs}
  value={state.value}
  onChange={value => setState({ value })}
/>
```
meduim
```js
initialState = { value: "download" };
const tabs = [
  { "label": "Загрузка: 12", "value": "download" },
  { "label": "В обработке: 32", "value": "processing" },
  { "label": "Проверено: 6", "value": "checked" }
];
<Tabs
  size="md"
  tabs={tabs}
  value={state.value}
  onChange={value => setState({ value })}
/>
```