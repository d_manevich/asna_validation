import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

export default class Tabs extends PureComponent {
  static propTypes = {
    tabs: PropTypes.arrayOf(PropTypes.shape({
      label: PropTypes.oneOfType([
        PropTypes.string, PropTypes.number
      ]).isRequired,
      value: PropTypes.oneOfType([
        PropTypes.string, PropTypes.number
      ]).isRequired
    })).isRequired,
    value: PropTypes.oneOfType([
      PropTypes.string, PropTypes.number
    ]).isRequired,
    onChange: PropTypes.func.isRequired,
    size: PropTypes.oneOf(['sm', 'md'])
  }

  static defaultProps = {
    size: 'sm'
  }

  render () {
    const { tabs, value, onChange, size } = this.props

    return (
      <div>
        {tabs.map(tab => (
          <Tab
            onClick={() => onChange(tab.value)}
            isActive={tab.value === value}
            size={size}
            key={tab.value}
          >
            {tab.label}
          </Tab>
        ))}
      </div>
    )
  }
}

const HEIGHT_SIZES = {
  sm: 32,
  md: 40
}
const WIDTH_SIZES = {
  sm: 190,
  md: 210
}

const Tab = styled.button`
  background-color: transparent;
  height: ${props => HEIGHT_SIZES[props.size]}px;
  width: ${props => WIDTH_SIZES[props.size]}px;
  outline: none;
  padding: 0;
  color: ${props => props.isActive ? 'white' : 'black'};
  border: ${props => props.isActive ? 'none' : '1px solid #3EC699'};
  background-image: ${props => props.isActive ? 'linear-gradient(135deg, #60CB8C, #25B3B3)' : 'none'};
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;

  & + & {
    border-left: none;
  }

  &:first-child {
    border-radius: 3px 0 0 3px;
  }

  &:last-child {
    border-radius: 0 3px 3px 0;
  }
`
