Light (200)
```jsx
  <FontsTest weight="200" />
```

Light (200) italic
```jsx
  <FontsTest weight="200" italic />
```

Regular (400)
```jsx
  <FontsTest weight="400" />
```

Regular (400) italic
```jsx
  <FontsTest weight="400" italic />
```

Semibold (600)
```jsx
  <FontsTest weight="600" />
```

Semibold (600) italic
```jsx
  <FontsTest weight="600" italic />
```

Bold (700)
```jsx
  <FontsTest weight="700" />
```

Bold (700) italic
```jsx
  <FontsTest weight="700" italic />
```
