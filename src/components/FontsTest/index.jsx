/* Only for font testing, don't use in production */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const FontsTest = ({ weight, italic }) => (
  <Text weight={weight} italic={italic}>AaBb</Text>
)

FontsTest.propTypes = {
  weight: PropTypes.string.isRequired,
  italic: PropTypes.bool
}

FontsTest.defaultProps = {
  italic: false
}

export default FontsTest

const Text = styled.span`
  font-size: 200px;
  font-weight: ${props => props.weight};
  font-style: ${props => props.italic ? 'italic' : 'normal'};
`
