import React, { PureComponent } from 'react'
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import styled from 'styled-components'
import { Route, Switch } from 'react-router-dom'
import Sidebar from '@/components/Sidebar'
import Templates from '@/pages/Templates'
import Template from '@/pages/Template'
import Logs from '@/pages/Logs'
import Loader from '@/pages/Loader'
import Upload from '@/pages/Upload'

class App extends PureComponent {
  render () {
    return (
      <Wrapper>
        <Sidebar
          pharmacyName='КОМФ-ОРТ ОФИС'
          profileEmail='a.moskalets@gmail.com'
          profileRole='Администратор'
        />
        <PageWrapper>
          <Page>
            <Switch>
              <Route path='/templates/new' component={Template} />
              <Route path='/templates/:id' component={Template} />
              <Route path='/templates' component={Templates} />
              <Route path='/logs' component={Logs} />
              <Route path='/loader'>
                <Switch>
                  <Route path='/loader/upload' component={Upload} />
                  <Route path='/loader' component={Loader} />
                </Switch>
              </Route>
            </Switch>
          </Page>
        </PageWrapper>
      </Wrapper>
    )
  }
}

export default DragDropContext(HTML5Backend)(App)

const Wrapper = styled.div`
  padding-left: 315px;
`

const PageWrapper = styled.div`
  width: 100%;
  padding: 0 110px 0 90px;
  background-color: #F2F2F3;
`

const Page = styled.div`
  width: 100%;
  max-width: 1200px;
  min-height: 100vh;
  margin: 0 auto;
`
