import { combineReducers } from 'redux'
import loaderReducer from '@/store/loader/loaderReducer'
import templatesReducer from '@/store/templates/templatesReducer'
import logsReducer from '@/store/logs/logsReducer'

export default combineReducers({
  loaderReducer,
  templatesReducer,
  logsReducer
})
