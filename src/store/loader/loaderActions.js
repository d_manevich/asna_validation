import * as at from './loaderConstants'

export const startUploadFiles = filenames => ({
  type: at.ADD_UPLOAD_FILES,
  payload: { filenames }
})

export const changeUploadProgress = (filenames, progress) => ({
  type: at.CHANGE_UPLOAD_PROGRESS,
  payload: { filenames, progress }
})

export const finishUploadFiles = filenames => ({
  type: at.REMOVE_UPLOAD_FILES,
  payload: { filenames }
})
