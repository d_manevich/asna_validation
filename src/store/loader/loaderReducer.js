import typeToReducer from 'type-to-reducer'
import * as at from './loaderConstants'

const initialState = {
  uploadedFiles: [],
  processedFiles: [],
  checkedFiles: []
}

export const initialFile = {
  name: '',
  progress: 0
}

export default typeToReducer({
  [ at.ADD_UPLOAD_FILES ]: (state, { payload: { filenames } }) => ({
    ...state,
    uploadedFiles: [
      ...state.uploadedFiles,
      ...filenames.map(name => ({ ...initialFile, name }))
    ]
  }),

  [ at.CHANGE_UPLOAD_PROGRESS ]: (state, { payload: { filenames, progress } }) => ({
    ...state,
    uploadedFiles: state.uploadedFiles.map(file => (
      filenames.includes(file.name) ? {...file, progress} : file
    ))
  }),

  [ at.REMOVE_UPLOAD_FILES ]: (state, { payload: { filenames } }) => ({
    ...state,
    uploadedFiles: state.uploadedFiles.filter(file => !filenames.includes(file.name))
  })
}, initialState)
