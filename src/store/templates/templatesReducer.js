import typeToReducer from 'type-to-reducer'
import * as at from './templatesConstants'
import { reject, findLastIndex, propEq, update } from 'ramda'

const initialState = {
  templates: []
}

export const initialTemplateState = {
  name: '',
  type: 'supply_chain', // or balances
  columns: [],
  active: false
}

export const initialColumnState = {
  name: '',
  active: true,
  requirements: [],
  optional: false
}

export default typeToReducer({
  [ at.LOAD_TEMPLATES ]: (state, action) => ({
    ...state,
    templates: action.payload.templates
  }),

  [ at.LOAD_TEMPLATE ]: (state, action) => {
    const newTemplate = action.payload.template
    const templateIndex = findLastIndex(propEq('id', newTemplate.id), state.templates)
    return {
      ...state,
      templates: ~templateIndex
        ? update(templateIndex, newTemplate, state.templates)
        : [...state.templates, newTemplate]
    }
  },

  [ at.SAVE_TEMPLATE ]: (state, action) => ({
    ...state,
    templates: [
      ...state.templates,
      {
        ...initialTemplateState,
        ...action.payload.template,
        id: Math.random().toString(36).substr(2, 16)
      }
    ]
  }),

  [ at.UPDATE_TEMPLATE ]: (state, action) => {
    const { template } = action.payload

    return {
      ...state,
      templates: state.templates.map(item => (
        item.id === template.id ? template : item
      ))
    }
  },

  [ at.DELETE_TEMPLATE ]: (state, action) => ({
    ...state,
    templates: reject(template => template.id === action.payload.id, state.templates)
  })
}, initialState)
