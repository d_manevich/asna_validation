import { sortBy, prop } from 'ramda'
import { initialTemplateState } from './templatesReducer'

export const getTemplates = state => sortBy(prop('name'), state.templatesReducer.templates)
export const getTemplate = (state, id) => (
  state.templatesReducer.templates.find(template => template.id === id) || initialTemplateState
)

export const hasActiveBalancesTemplate = state => (
  !!state.templatesReducer.templates.find(template => template.active && template.type === 'balances')
)
export const hasActiveSupplyChainTemplate = state => (
  !!state.templatesReducer.templates.find(template => template.active && template.type === 'supply_chain')
)
