import * as at from './templatesConstants'

export const loadTemplates = templates => ({
  type: at.LOAD_TEMPLATES,
  payload: { templates }
})

export const loadTemplate = template => ({
  type: at.LOAD_TEMPLATE,
  payload: { template }
})

export const createTemplate = template => ({
  type: at.SAVE_TEMPLATE,
  payload: { template }
})

export const updateTemplate = template => ({
  type: at.UPDATE_TEMPLATE,
  payload: { template }
})

export const deleteTemplate = id => ({
  type: at.DELETE_TEMPLATE,
  payload: { id }
})
