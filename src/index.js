import React from 'react'
import ReactDOM from 'react-dom'
import configureStore from '@/store/configureStore'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import App from '@/App'
import registerServiceWorker from '@/registerServiceWorker'
import '@/assets/fonts/fonts.css'
import '@/index.css'

const { persistor, store } = configureStore()

ReactDOM.render((
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <Switch>
          <Route path='/' component={App} />
        </Switch>
      </BrowserRouter>
    </PersistGate>
  </Provider>
), document.getElementById('root'))
registerServiceWorker()
